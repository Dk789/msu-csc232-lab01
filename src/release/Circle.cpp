/**
 * CSC232 Data Structures with C++
 * Missouri State University, Spring 2017.
 *
 * @file    Circle.cpp
 * @authors Jim Daehn <jdaehn@missouristate.edu>
 *          Your Name <fillmein@missouristate.edu>
 *          Partner Name <partnerid@missouristate.edu>
 * @brief   Circle implemention.
 */

#include "Circle.h"

namespace csc232 {

  /* Constructors */
  Circle::Circle() : m_radius(1.0) {
    // no-op
  }

  // TODO: modify initializer to prevent storing radius <= 0.
  Circle::Circle(const double radius) : m_radius(radius) {
    // no-op
  }

  /* Getter/Setters */
  double Circle::getRadius() const {
    // FIXME: Return the appropriate value
    return 0;
  }
  
  void Circle::setRadius(const double radius) {
    // TODO: Implement me
  }

  /* Overrides */
  double Circle::getPerimeter() const {
    // FIXME: Return the appropriate value
    return 0;
  }
  
  double Circle::getArea() const {
    // FIXME: Return the appropriate value
    return 0;
  }

  /* Additional operations */
  double Circle::getCircumference() const {
    // FIXME: Return the appropriate value
    return 0;
  }

}
